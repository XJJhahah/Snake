package moonsnake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Mpanel extends JPanel implements KeyListener, ActionListener{
    ImageIcon title=new ImageIcon("title.png"); //添加题目图片
    ImageIcon body=new ImageIcon("body.jpg");
    ImageIcon up=new ImageIcon("up.jpg");
    ImageIcon down=new ImageIcon("down.jpg");
    ImageIcon left=new ImageIcon("left.jpg");
    ImageIcon right=new ImageIcon("right.jpg");
    ImageIcon food=new ImageIcon("fool.jpg");

    int len=3;//默认身体是三
    int score=0;//默认分数是0
    int[] snakex=new int[750];//最长是700
    int[] snakey=new int[750];//最长是700
    String fx="R"; //方向默认向右
    boolean isStarted = false;//定义开始默认为false
    Timer timer=new Timer(100,  this);//设置时钟,每100毫秒实现一次
    int foodx;//食物x轴
    int foody;//食物y轴
    Random rand=new Random();//随机对象
    boolean isFalled=false;//结束游戏对象

    public Mpanel(){
        InitSnake();
        this.setFocusable(true);//可以获取键盘时间
        this.addKeyListener(this);//自己可以处理键盘时间
        timer.start();//启动时钟
    }
    public void paintComponent(Graphics g){//增加画笔
        super.paintComponent(g);
        this.setBackground(Color.white); //画布颜色
        title.paintIcon(this,g,25,11);
        //增加黑色框框
        g.fillRect(25,75,850,600);
        g.setColor(Color.WHITE);
        g.drawString("长度："+len,750,35);
        g.drawString("分数："+score,750,50);

        //打印会转动的头
        if (fx=="R"){
            right.paintIcon(this,g,snakex[0],snakey[0]);
        }else if(fx=="L"){
            left.paintIcon(this,g,snakex[0],snakey[0]);
        }else if(fx=="D"){
            down.paintIcon(this,g,snakex[0],snakey[0]);
        }else if(fx=="U"){
            up.paintIcon(this,g,snakex[0],snakey[0]);
        }

        for(int i=1;i<len;i++){//打印蛇的身体
            body.paintIcon(this,g,snakex[i],snakey[i]);
        }
        food.paintIcon(this,g,foodx,foody);//画食物

        //加入开始请按空格按钮
        if (isStarted==false){
            g.setColor(Color.WHITE);//字体颜色
            g.setFont(new Font("楷体",Font.BOLD,40));
            g.drawString("请按空格开始",300,300);
        }
        if (isFalled){
            g.setColor(Color.RED);//字体颜色
            g.setFont(new Font("楷体",Font.BOLD,40));
            g.drawString("游戏结束请按空格重新开始游戏",300,300);
        }
    }
    public void InitSnake(){
        //初始化蛇
        len=3;
        score=0;
        snakex[0]=100;
        snakey[0]=100;
        snakex[1]=75;
        snakey[1]=100;
        snakex[2]=50;
        snakey[2]=100;
        foodx=25+25*rand.nextInt(34);
        foody=75+25*rand.nextInt(24);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode=e.getKeyCode();
        if(keyCode==KeyEvent.VK_SPACE){  //键盘输入等于空格键
            if (isFalled){
                isFalled = false;
                InitSnake();
            }else {
                isStarted=!isStarted;
            }
            repaint();//重新画，刷新
        //改变头的值
        }else if (keyCode ==KeyEvent.VK_LEFT){
            fx="L";
        }else if (keyCode ==KeyEvent.VK_RIGHT){
            fx="R";
        }else if (keyCode ==KeyEvent.VK_UP){
            fx="U";
        }else if (keyCode ==KeyEvent.VK_DOWN){
            fx="D";
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (isStarted&!isFalled){//当键盘按空格才开始,如果停止
            //身体移动
            for(int i=len-1;i>0;i--){
                snakex[i]=snakex[i-1];
                snakey[i]=snakey[i-1];
            }
            //头部移动
            if (fx=="R"){
                snakex[0]=snakex[0]+25;//头移动
                if(snakex[0]>850){//防止虫子跳出边框
                    snakex[0]=25;
                }
            }else if(fx=="L"){
                snakex[0]=snakex[0]-25;//头移动
                if(snakex[0]<25){//防止虫子跳出边框
                    snakex[0]=850;
                }
            }else if(fx=="U") {
                snakey[0] = snakey[0] - 25;//头移动
                if (snakey[0] < 75 ) {//防止虫子跳出边框
                    snakey[0] = 650;
                }
            }else if(fx=="D") {
                snakey[0] = snakey[0] + 25;//头移动
                if (snakey[0] >670 ){//防止虫子跳出边框
                    snakey[0] =75;
                }
            }
            //蛇吃到了东西
            if (snakex[0]==foodx&&snakey[0]==foody){
                len++;
                score=score+10;
                foodx=25+25*rand.nextInt(34);
                foody=75+25*rand.nextInt(24);

            }
            for (int i=1;i<len;i++){
                if(snakex[i]==snakex[0]&&snakey[i]==snakey[0]){
                    isFalled=true;
                }
            }

            repaint();
        }
        timer.start();
    }
}
