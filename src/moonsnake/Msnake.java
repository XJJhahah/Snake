package moonsnake;

import javax.swing.*;

public class Msnake extends JFrame {
    public Msnake(){
        setTitle("贪吃蛇");
        setBounds(10,10,900,720);
        setResizable(false);//不可拖动屏幕
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//关掉结束进程
    }
    public static void main(String[] args) {
        JFrame frame=new Msnake();
        JPanel panel=new Mpanel();
        frame.add(panel);

        frame.setVisible(true);//显示窗体

    }

}
